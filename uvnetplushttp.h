#pragma once

namespace uvNetPlus {
namespace Http {

class CHttpMsg;
class CHttpRequest;
class CHttpResponse;
class CHttpClient;
class CHttpServer;

#ifdef WIN32
typedef std::unordered_multimap<std::string, std::string> hash_list;
#else
typedef std::multimap<std::string, std::string> hash_list;
#endif

enum METHOD {
    OPTIONS = 0,
    HEAD,
    GET,
    POST,
    PUT,
    DEL,
    TRACE,
    CONNECT
};

enum VERSION {
    HTTP1_0 = 0,
    HTTP1_1,
    HTTP2,
    HTTP3
};

class CHttpMsg {
public:
    bool aborted;   //请求终止时设置为true
    bool complete;  //http消息接收完整时设置为true

    METHOD      method;     // 请求方法
    std::string path;        // 请求路径

    int         statusCode;     //应答状态码
    std::string statusMessage;  //应答状态消息

    VERSION     version;        //http版本号 1.1或1.0
    std::string rawHeaders;     //完整的头部字符串
    std::string rawTrailers;    //完整的尾部字符串
    hash_list   headers;        //解析好的http头部键值对
    hash_list   trailers;       //解析好的http尾部键值对
    bool        keepAlive;      // 是否使用长连接, true时，使用CTcpConnPool管理连接
    bool        chunked;        // Transfer-Encoding: chunked
    uint32_t    contentLen;     // chunked为false时：内容长度；chunked为true时，块长度
    std::string content;        // 一次的接收内容

    CHttpMsg();
    ~CHttpMsg();
};

class CHttpConnect {
public:
    CHttpConnect();
    ~CHttpConnect();

    /**
     * 显示填写http头，调用后隐式http头的接口就无效了
     * @param headers http头域完整字符串，包含每一行结尾的"\r\n"
     */
    virtual void WriteHead(std::string headers);

    /**
     * 获取已经设定的隐式头
     */
    virtual std::vector<std::string> GetHeader(std::string name);

    /**
     * 获取所有设定的隐式头的key
     */
    virtual std::vector<std::string> GetHeaderNames();

    /**
     * 隐式头是否已经包含一个名称
     */
    virtual bool HasHeader(std::string name);

    /**
     * 移除一个隐式头
     */
    virtual void RemoveHeader(std::string name);

    /**
     * 重新设置一个头的值，或者新增一个隐式头
     * param name field name
     * param value 单个field value
     * param values 以NULL结尾的字符串数组，多个field value
     */
    virtual void SetHeader(std::string name, std::string value);
    virtual void SetHeader(std::string name, char **values);

    /**
     * 设置内容长度。内容分多次发送，且不使用chunked时使用。
     */
    virtual void SetContentLen(uint32_t len);

    /**
     * 查看是否完成
     */
    virtual bool Finished();

public:
     CTcpSocket        *tcpSocket;

protected:
    /** 由隐式头组成字符串 */
    std::string getImHeaderString();

protected:
    std::string         m_strHeaders;   // 显式的头
    hash_list           m_Headers;      // 隐式头的内容
    bool                m_bHeadersSent; // header是否已经发送
    bool                m_bFinished;    // 发送是否完成
    uint32_t            m_nContentLen;  // 设置内容的长度
};

class CHttpRequest : public CHttpConnect {
    typedef void(*ErrorCB)(CHttpRequest *req, std::string error);
    typedef void(*ResCB)(CHttpRequest *req, CHttpMsg* response);
public:
    typedef void(*DrainCB)(CHttpRequest *req);

    PROTOCOL            protocol;  // 协议,http或https
    METHOD              method;    // 方法
    std::string         path;      // 请求路径
    VERSION             version;   // http版本号 1.0或1.1
    std::string         host;      // 域名或IP
    int                 port;      // 端口
    std::string         localaddr; // 指定本地IP，默认为空
    int                 localport; // 指定本地端口， 默认为0。只有很特殊的情形需要设置，正常都不需要
    bool                keepAlive; // 是否使用长连接, true时，使用CTcpConnPool管理连接
    bool                chunked;   // Transfer-Encoding: chunked
    void               *usrData;   // 用户自定义数据
    bool                autodel;   // 接收完成后自动删除，不需要手动释放。
    uint64_t            fd;        // SOCKET的值(测试用)


    /** 客户端收到connect方法的应答时回调 */
    ResCB OnConnect;
    /** 客户端收到1xx应答(101除外)时回调 */
    ResCB OnInformation;
    /** 客户端收到101 upgrade 时回调 */
    ResCB OnUpgrade;
    /** 客户端收到应答时回调，如果是其他指定回调，则不会再次进入这里 */
    ResCB OnResponse;


    ErrorCB     OnError;        // 发生错误
    DrainCB     OnDrain;        // 发送数据完成

    /** 删除实例 */
    virtual void Delete() = 0;

    /**
     * 用来发送一块数据，如果chunked=true，发送一个chunk的数据
     * 如果chunked=false，使用这个方法多次发送数据，必须自己在设置头里设置length
     * @param chunk 需要发送的数据
     * @param len 发送的数据长度
     * @param cb 数据写入缓存后调用
     */
    virtual bool Write(const char* chunk, int len, DrainCB cb = NULL) = 0;

    /**
     * 完成一个发送请求，如果有未发送的部分则将其发送，如果chunked=true，额外发送结束段'0\r\n\r\n'
     * 如果chunked=false,协议头没有发送，则自动添加length
     */
    virtual bool End() = 0;

    /**
     * 相当于Write(data, len, cb);end();
     */
    virtual void End(const char* data, int len) = 0;
protected:
    CHttpRequest();
    virtual ~CHttpRequest() = 0;
};

class CHttpClient {
public:
    typedef void(*ReqCB)(CHttpRequest *req, void* usr, std::string error);

    /**
     * 创建一个http客户端环境
     * @param net 环境句柄
     * @param maxConns 同一个地址最大连接数
     * @param maxIdle 同一个地址最大空闲连接
     * @param timeOut 空闲连接超时时间
     * @param maxRequest 同一个地址请求最大缓存
     */
    CHttpClient(CNet* net, uint32_t maxConns=512, uint32_t maxIdle=100, uint32_t timeOut=20, uint32_t maxRequest=0);
    ~CHttpClient();
    bool Request(std::string host, int port, void* usr = NULL, ReqCB cb = NULL);

    /**
     * 默认请求获取成功回调函数，如果Request设置了指定回调，则优先使用指定的回调
     */
    ReqCB                OnRequest;
    CTcpConnPool        *connPool;

#ifdef USE_TLS
    /**
     * 使用tls
     */
    void UseTls(bool verify = false);
    /**
     * 指定一个自定义的CA
     */
    void SetTlsCa(const uint8_t *buff, uint64_t buffLen);
    /**
     * 添加多个CA文件
     * @path pem文件的路径，字符串的数组，以NULL结尾
     */
    void SetTlsCaFile(const char **path);
    /**
     * 添加指定目录下所有的CA文件
     * @path 目录路径，字符串的数组，以NULL结尾
     */
    void SetTlsCaPath(const char **path);
#endif
};

class CHttpResponse : public CHttpConnect {
public:
    typedef void(*ResCb)(CHttpResponse *response);

    bool                sendDate;      // 默认true，在发送头时自动添加Date头(已存在则不会添加)
    int                 statusCode;    // 状态码
    std::string         statusMessage; //自定义的状态消息，如果为空，发送时会取标准消息
    VERSION             version;       // http版本号 1.0或1.1
    bool                keepAlive; // 是否使用长连接, true时，使用CTcpConnPool管理连接
    bool                chunked;   // Transfer-Encoding: chunked

    /** 发送完成前,socket中断了会回调该方法 */
    ResCb OnClose;
    /** 应答发送完成时回调，所有数据都已经发送 */
    ResCb OnFinish;

    /**
     * 添加一个尾部数据
     * @param key 尾部数据的field name，这个值已经在header中的Trailer里定义了
     * @param value 尾部数据的field value
     */
    virtual void AddTrailers(std::string key, std::string value) = 0;

    /**
     * Sends a HTTP/1.1 100 Continue message。包括write和end的功能
     */
    virtual void WriteContinue() = 0;

    /**
     * Sends a HTTP/1.1 102 Processing message to the client
     */
    virtual void WriteProcessing() = 0;

    /**
     * 显示填写http头，调用后隐式http头的接口就无效了
     * @param statusCode 响应状态码
     * @param statusMessage 自定义状态消息，可以为空，则使用标准消息
     * @param headers http头域完整字符串，每行都要包含"\r\n"
     */
    virtual void WriteHead(int statusCode, std::string statusMessage, std::string headers) = 0;

    /**
     * 如果调用了此方法，但没有调用writeHead()，则使用隐式头并立即发送头
     */
    virtual void Write(const char* chunk, int len, ResCb cb = NULL) = 0;

    /**
     * 表明应答的所有数据都已经发送。每个实例都需要调用一次end。执行后会触发OnFinish
     */
    virtual void End() = 0;

    /**
     * 相当于调用write(data, len, cb) ; end()
     */
    virtual void End(const char* data, int len, ResCb cb = NULL) = 0;

protected:
    CHttpResponse();
    virtual ~CHttpResponse() = 0;
};

class CHttpServer {
protected:
    typedef void (*EventCB)(CHttpServer* svr, std::string err);
    typedef void(*ReqCb)(CHttpServer *server, CHttpMsg *request, CHttpResponse *response);
public:
    /** 监听开启成功 */
    EventCB OnListen;
    /** 接受到一个包含'Expect: 100-continue'的请求时调用，如果没有指定，自动发送'100 Continue' */
    ReqCb OnCheckContinue;
    /** 接收到一个包含Expect头，但不是100的请求时调用，如果没有指定，自动发送'417 Expectation Failed' */
    ReqCb OnCheckExpectation;
    /** 收到upgrade请求时调用 */
    ReqCb OnUpgrade;
    /** 接收到一个请求，如果是其他指定的回调，就不会进入这里 */
    ReqCb OnRequest;

    /** 创建一个实例 */
    static CHttpServer* Create(CNet* net);
    /** 设置请求path匹配的回调，优先级比OnRequest高 */
    virtual void On(std::string path, ReqCb cb) = 0;
    /** 设备长连接保活时间，超过保活时间而没有新请求则断开连接 */
    virtual void SetKeepAlive(uint32_t secends) = 0;
    /** 服务器启动监听 */
    virtual bool Listen(std::string strIP, uint32_t nPort) = 0;
    /** 服务器关闭 */
    virtual void Close() = 0;
    /** 服务器是否在监听连接 */
    virtual bool Listening() = 0;

#ifdef USE_TLS
	/**
	 * 开启Tls
	 */
    virtual void UseTls() = 0;

	/**
	 * 设置CA内容
	 */
	virtual void SetCA(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置CA文件
	 */
	virtual void SetCAFile(std::string path) = 0;

	/**
	 * 设置证书内容
	 */
	virtual void SetCrt(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置证书文件
	 */
	virtual void SetCrtFile(std::string path) = 0;

	/**
	 * 设置私钥内容
	 */
	virtual void SetKey(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置私钥文件
	 */
	virtual void SetKeyFile(std::string path) = 0;
#endif
protected:
    CHttpServer();
    virtual ~CHttpServer() = 0;
};
}; //namespace Http
}