#include "util.h"
#include "utilc.h"
#include "uvnethttp.h"
#include "uvnetsoap.h"
#include "pugixml.hpp"
#include <set>
#include <sstream>

namespace uvNetPlus {
namespace Http {
    bool CWsdl::ParseResponse() {
        bool ret = false;
        const char *tmp = NULL;
        pugi::xml_document doc;
        do {
            if(!doc.load(m_recvData.c_str()))
                break;

            // 根节点
            pugi::xml_node root = doc.child("wsdl:definitions");
            if (!root)
                break;
            // 遍历属性
            for(pugi::xml_node::attribute_iterator iter = root.attributes_begin();
                iter != root.attributes_end(); ++iter){
                    if(m_mapRootAttributes.count(iter->name()) == 0) {
                        m_mapRootAttributes.insert(make_pair(iter->name(), iter->value()));
                    }
            }

            //遍历子节点
            for (pugi::xml_node node = root.first_child(); node; node = node.next_sibling()) {
                const char *nodeName  = node.name();
                if(!strcasecmp(nodeName, "wsdl:message")) {
                    WsdlMessage *msg = new WsdlMessage();
                    msg->name = node.attribute("name").value();
                    for(pugi::xml_node part = node.first_child(); part; part = part.next_sibling()) {
                        string name = part.attribute("name").value();
                        string type = part.attribute("type").value();
                        msg->parts.insert(make_pair(name, type));
                    }
                    m_mapMessages.insert(make_pair(msg->name, msg));
                } else if(!strcasecmp(nodeName, "wsdl:portType")) {
                    WsdlPortType *portType = new WsdlPortType();
                    portType->name = node.attribute("name").value();
                    for(pugi::xml_node operation = node.first_child(); operation; operation = operation.next_sibling()) {
                        WsdlPortType::WsdlOperation *portOperation = new WsdlPortType::WsdlOperation();
                        portOperation->name = operation.attribute("name").value();
                        pugi::xml_node input = operation.child("wsdl:input");
                        portOperation->input.name = input.attribute("name").value();
                        tmp = input.attribute("message").value();
                        portOperation->input.message = strncasecmp(tmp, "impl:", 5) ? tmp : tmp+5;
                        pugi::xml_node output = operation.child("wsdl:output");
                        portOperation->output.name = output.attribute("name").value();
                        tmp = output.attribute("message").value();
                        portOperation->output.message = strncasecmp(tmp, "impl:", 5) ? tmp : tmp+5;
                        portType->operations.insert(make_pair(portOperation->name, portOperation));
                    }
                    m_mapPortTypes.insert(make_pair(portType->name, portType));
                } else if(!strcasecmp(nodeName, "wsdl:binding")) {
                    WsdlBinding* binding = new WsdlBinding();
                    binding->name = node.attribute("name").value();
                    tmp = node.attribute("type").value();
                    binding->type = strncasecmp(tmp, "impl:", 5) ? tmp : tmp + 5;
                    for(pugi::xml_node operation = node.first_child(); operation; operation = operation.next_sibling()) {
                        if(strcasecmp(operation.name(), "wsdl:operation"))
                            continue;
                        WsdlBinding::WsdlOperation *bindOperation = new WsdlBinding::WsdlOperation();
                        bindOperation->name = operation.attribute("name").value();
                        pugi::xml_node input = operation.child("wsdl:input");
                        bindOperation->input.name = input.attribute("name").value();
                        pugi::xml_node inputBody = input.child("wsdlsoap:body");
                        bindOperation->input.use = inputBody.attribute("use").value();
                        bindOperation->input.encodingStyle = inputBody.attribute("encodingStyle").value();
                        bindOperation->input.nameSpace = inputBody.attribute("namespace").value();
                        pugi::xml_node output = operation.child("wsdl:output");
                        bindOperation->output.name = output.attribute("name").value();
                        pugi::xml_node outputBody = output.child("wsdlsoap:body");
                        bindOperation->output.use = outputBody.attribute("use").value();
                        bindOperation->output.encodingStyle = outputBody.attribute("encodingStyle").value();
                        bindOperation->output.nameSpace = outputBody.attribute("namespace").value();
                        binding->operations.insert(make_pair(bindOperation->name, bindOperation));
                    }
                    m_mapBinds.insert(make_pair(binding->name, binding));
                } else if(!strcasecmp(nodeName, "wsdl:service")) {
                    WsdlService* service = new WsdlService();
                    service->name = node.attribute("name").value();
                    for(pugi::xml_node portNode = node.first_child(); portNode; portNode = portNode.next_sibling()) {
                        if(!strcasecmp(portNode.name(), "wsdl:port")) {
                            WsdlService::WsdlPort *port = new WsdlService::WsdlPort();
                            port->name = portNode.attribute("name").value();
                            tmp = portNode.attribute("binding").value();
                            port->bindingName = strncasecmp(tmp, "impl:", 5) ? tmp : tmp+5;
                            pugi::xml_node addrNode = portNode.child("wsdlsoap:address");
                            port->address = addrNode.attribute("location").value();
                            service->ports.insert(make_pair(port->name, port));
                        }
                    }
                    m_mapServices.insert(make_pair(service->name, service));
                }
            }
            ret = true;
        }while (0);

        return ret;
    }

    struct SoapRequestData {
        void *user;
        CUNSoapRequest  *req;
        CUNSoapResponse *res;
        CSoapRequest::RequestCB cb;
        std::string         reqData;
        std::string         resData;

        ~SoapRequestData(){
            SAFE_DELETE(res);
        }
        bool ParseResponse() {
            bool ret = false;
            const char *tmp = NULL;
            pugi::xml_document doc;
            do {
                if(!doc.load(resData.c_str()))
                    break;

                const char* tmp = NULL;
                // 根节点
                for (pugi::xml_node root = doc.first_child(); root; root = root.next_sibling()) {
                    const char* rootname = root.name();
                    tmp = strchr(rootname,':');
                    tmp = tmp==NULL ? rootname : tmp+1;
                    if(!strcasecmp(tmp, "Envelope")) {
                        //根节点
                        for (pugi::xml_node node = root.first_child(); node; node = node.next_sibling()) {
                            const char *bodyname = node.name();
                            tmp = strchr(bodyname,':');
                            tmp = tmp==NULL ? bodyname : tmp+1;
                            if(!strcasecmp(tmp, "Body")) {
                                //body节点
                                for (pugi::xml_node opration = node.first_child(); opration; opration = opration.next_sibling()) {
                                    const char *oprationName = opration.name();
                                    tmp = strchr(oprationName,':');
                                    tmp = tmp==NULL ? oprationName : tmp+1;
                                    if(!strcasecmp(tmp, req->pPortOperation->output.name.c_str())) {
                                        for(pugi::xml_node output = opration.first_child(); output; output = output.next_sibling()) {
                                            res->resParams.insert(make_pair(output.name(), output.child_value()));
                                        }
                                        ret = true;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }while(0);

            return ret;
        }
    };

    //////////////////////////////////////////////////////////////////////////

    CSoapRequest::CSoapRequest()
        : client(NULL)
        , userData(NULL)
    {}

    CSoapRequest::~CSoapRequest(){}

    CUNSoapRequest::CUNSoapRequest()
        : pBindingOperation(NULL)
        , pPortOperation(NULL)
    {
        m_xmlnsSOAPENV = "http://schemas.xmlsoap.org/soap/envelope/";
        m_xmlnsSOAPENC = "http://schemas.xmlsoap.org/soap/encoding/";
        m_xmlnsXsi = "http://www.w3.org/2001/XMLSchema-instance";
        m_xmlnsXsd = "http://www.w3.org/2001/XMLSchema";
    }

    bool CUNSoapRequest::SetParam(std::string name, std::string value, CODE_METHOD codeMethod) {
        if(m_mapParams.count(name)>0){
            Log::error("repeat param");
            return false;
        }
        if(codeMethod == CODE_METHOD_TRANS) {
            std::string tmp;
            for(auto c:value) {
                if(c == '<')
                    tmp.append("&lt;");
                else if(c == '>')
                    tmp.append("&gt;");
                else if(c == '&')
                    tmp.append("&amp;");
                else if(c == '\'')
                    tmp.append("&apos;");
                else if(c == '"')
                    tmp.append("&quot;");
                else
                    tmp.push_back(c);
            }
            value = tmp;
        } else if (codeMethod == CODE_METHOD_CDATA) {
            value = "<![CDATA[" + value + "]]>";
        }
        m_mapParams.insert(make_pair(name, value));
        return true;
    }

    void CUNSoapRequest::Request(CSoapRequest::RequestCB cb, void* usrData) {
        SoapRequestData *reqData = new SoapRequestData();
        reqData->user = usrData;
        reqData->req  = this;
        reqData->res  = new CUNSoapResponse();
        reqData->cb   = cb;

        reqData->reqData.append(m_strEnvelopeHead);
        for(auto param : m_mapParams) {
            reqData->reqData.append("<");
            reqData->reqData.append(param.first);
            reqData->reqData.append(">");
            reqData->reqData.append(param.second);
            reqData->reqData.append("</");
            reqData->reqData.append(param.first);
            reqData->reqData.append(">");
        }
        reqData->reqData.append(m_strEnvelopeTail);
        m_mapParams.clear();

        CUNSoapClient* pSoapClient = (CUNSoapClient*)client;
        pSoapClient->m_pHttpClient->Request(m_strHost, m_nPort, reqData, [](CHttpRequest *req, void* usr, std::string error) {
            SoapRequestData* reqData = (SoapRequestData*)usr;
            if(!error.empty()){
                //失败了
                if(reqData->cb) {
                    reqData->cb((CSoapRequest*)reqData, NULL, error);
                }
                delete reqData;
                return;
            };
            CUNSoapRequest *soapReq = (CUNSoapRequest*)reqData->req;
            req->usrData = reqData;
            req->method = POST;
            req->path = soapReq->m_strPath;
            req->OnResponse = [](CHttpRequest *req, CHttpMsg* response) {
                SoapRequestData* reqData = (SoapRequestData*)req->usrData;
                reqData->resData += response->content;
                if(!response->complete) {
                    return;
                }

                if(response->statusCode != 200) {
                    if(reqData->cb) {
                        reqData->cb(reqData->req, NULL, response->statusMessage);
                    }
                } else {
                    //解析应答
                    bool parse = reqData->ParseResponse();
                    if(reqData->cb) {
                        if(parse)
                            reqData->cb(reqData->req, reqData->res, "");
                        else
                            reqData->cb(reqData->req, reqData->res, "parse response failed");
                    }
                }
                delete reqData;
            };
            req->OnError = [](CHttpRequest *req, std::string error) {
                SoapRequestData* reqData = (SoapRequestData*)req->usrData;
                if(reqData->cb) {
                    reqData->cb(reqData->req, NULL, error);
                }
                delete reqData;
            };
            req->SetHeader("Content-Type", "text/xml; charset=utf-8");
            req->SetHeader("SOAPAction", "");
            req->End(reqData->reqData.c_str(), reqData->reqData.size());
        });
    }

    //////////////////////////////////////////////////////////////////////////

    CSoapResponse::CSoapResponse(){}

    CSoapResponse::~CSoapResponse(){}

    CUNSoapResponse::CUNSoapResponse(){}

    //////////////////////////////////////////////////////////////////////////

    CSoapClient::~CSoapClient() {}

    void CSoapClient::Create(CHttpClient *http, std::string url, ParseCB cb, void *user /*= NULL*/) {
        
    }

    void CSoapClient::Create(CHttpClient *http, std::string host, int port, string path, ParseCB cb, void *user /*= NULL*/) {
        CUNSoapClient* soapClient = new CUNSoapClient();
        soapClient->m_pHttpClient = http;
        soapClient->m_funParseWsdlCB = cb;
        soapClient->userData = user;
        soapClient->m_strHost = host;
        soapClient->m_nPort = port;
        soapClient->m_strPath = path;

        http->Request(host, port, soapClient, [](CHttpRequest *req, void* usr, std::string error) {
            CUNSoapClient* soapClient = (CUNSoapClient*)usr;
            if(!error.empty()){
                //失败了
                if(soapClient->m_funParseWsdlCB) {
                    soapClient->m_funParseWsdlCB((CSoapClient*)soapClient, error);
                }
                return;
            }
            req->usrData = soapClient;
            req->method = GET;
            req->path = soapClient->m_strPath + "?WSDL";
            req->OnResponse = [](CHttpRequest *req, CHttpMsg* response) {
                CUNSoapClient* soapClient = (CUNSoapClient*)req->usrData;
                if(response->statusCode != 200) {
                    if(soapClient->m_funParseWsdlCB) {
                        soapClient->m_funParseWsdlCB((CSoapClient*)soapClient, response->statusMessage);
                    }
                    return;
                }
                soapClient->m_recvData += response->content;
                if(response->complete) {
                    if(soapClient->ParseResponse()) {
                        //wsdl解析成功
                        if(soapClient->m_funParseWsdlCB) {
                            soapClient->m_funParseWsdlCB((CSoapClient*)soapClient, "");
                        }
                    } else {
                        if(soapClient->m_funParseWsdlCB) {
                            soapClient->m_funParseWsdlCB((CSoapClient*)soapClient, "wsdl parse failed");
                        }
                    }
                    soapClient->m_recvData.clear();
                }
            };
            req->OnError = [](CHttpRequest *req, std::string error) {
                CUNSoapClient* soapClient = (CUNSoapClient*)req->usrData;
                if(soapClient->m_funParseWsdlCB) {
                    soapClient->m_funParseWsdlCB((CSoapClient*)soapClient, error);
                }
            };
            req->End();
        });
    }

    //////////////////////////////////////////////////////////////////////////

    CUNSoapClient::CUNSoapClient() {

    }

    CUNSoapClient::~CUNSoapClient() {

    }

    void CUNSoapClient::Delete() {

    }

    CSoapRequest* CUNSoapClient::Request(std::string service, std::string port, std::string operation) {
        WsdlService *pService = NULL;
        WsdlService::WsdlPort *pPortType = NULL;
        WsdlBinding *pBinding = NULL;
        WsdlBinding::WsdlOperation *pBindingOperation = NULL;
        WsdlPortType *pPort = NULL;
        WsdlPortType::WsdlOperation *pPortOperation = NULL;

        if(m_mapServices.count(service)==0) {
            return NULL;
        }
        pService = m_mapServices[service];

        if(pService->ports.count(port)==0) {
            return NULL;
        }
        pPortType = pService->ports[port];

        if(m_mapBinds.count(pPortType->bindingName)==0) {
            return NULL;
        }
        pBinding = m_mapBinds[pPortType->bindingName];
        if(pBinding->operations.count(operation)==0) {
            return NULL;
        }
        pBindingOperation = pBinding->operations[operation];

        if(m_mapPortTypes.count(pBinding->type)==0){
            return NULL;
        }
        pPort = m_mapPortTypes[pBinding->type];
        if(pPort->operations.count(operation)==0) {
            return NULL;
        }
        pPortOperation = pPort->operations[operation];

        CUNSoapRequest *req = new CUNSoapRequest();
        req->client = this;
        req->operation = operation;
        req->m_xmlnsNamespace = pBindingOperation->input.nameSpace;
        req->m_strHost = m_strHost;
        req->m_nPort = m_nPort;
        req->m_strPath = m_strPath; 
        req->pPortOperation = pPortOperation;
        req->pBindingOperation = pBindingOperation;

        stringstream ss;
        ss << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><soapenv:Envelope xmlns:xsi=\""
           << req->m_xmlnsXsi
           << "\" xmlns:xsd=\""
           << req->m_xmlnsXsd
           << "\" xmlns:soapenv=\""
           << req->m_xmlnsSOAPENV
           << "\" xmlns:ns1=\""
           << req->m_xmlnsNamespace
           << "\">"
           << "<soapenv:Body><ns1:"
           << operation
           << " soapenv:encodingStyle=\""
           << pBindingOperation->input.encodingStyle
           << "\">";
        req->m_strEnvelopeHead = ss.str();

        ss.str("");
        ss.clear();
        ss << "</ns1:" << operation << ">"
            "</soapenv:Body>"
            "</soapenv:Envelope>";
        req->m_strEnvelopeTail = ss.str();

        return req;
    }

    //////////////////////////////////////////////////////////////////////////

    CSoapServer::~CSoapServer(){}

    void CSoapServer::Create(CHttpServer *http, char* wsdl, std::string path, ParseCB cb, void *user /*= NULL*/) {
        uv_fs_t openReq, readReq, closeReq;
        int ret = uv_fs_open(NULL, &openReq, wsdl, 0, 0, NULL);
        if(ret < 0) {
            Log::error("uv_fs_open %s failed:%s", wsdl, uv_strerror(ret));
            if(cb) {
                cb(NULL, "wsdl parse failed");
            }
            return;
        }

        char* pBuf = (char*)malloc(1024);
        uv_buf_t buf = uv_buf_init(pBuf, 1024);
        int64_t offset = 0;
        string content;
        do{
            ret = uv_fs_read(NULL, &readReq, openReq.result, &buf, 1, offset, NULL);
            if(ret < 0) {
                Log::error("uv_fs_read %s failed:%s", wsdl, uv_strerror(ret));
                break;
            }
            offset += readReq.result;
            content.append(pBuf, readReq.result);
            uv_fs_req_cleanup(&readReq);
        } while (readReq.result > 0);


        uv_fs_close(NULL, &closeReq, openReq.result, NULL);
        uv_fs_req_cleanup(&closeReq);

        uv_fs_req_cleanup(&openReq);


        Create(http, content, path, cb, user);
    }

    void CSoapServer::Create(CHttpServer *http, std::string wsdl, std::string path, ParseCB cb, void *user /*= NULL*/) {
        CUNSoapServer* soapServer = new CUNSoapServer();
        soapServer->m_pHttpServer = http;
        soapServer->m_recvData = wsdl;
        soapServer->m_funParseWsdlCB = cb;
        soapServer->userData = user;
        if(soapServer->ParseResponse()) {
            //wsdl解析成功
            if(cb) {
                cb((CSoapServer*)soapServer, "");
            }

            http->On(path+"?wsdl", [](CHttpServer *server, CHttpMsg *request, CHttpResponse *response){});
            http->On(path,[](CHttpServer *server, CHttpMsg *request, CHttpResponse *response){});
        } else {
            if(cb) {
                cb((CSoapServer*)soapServer, "wsdl parse failed");
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////

    CUNSoapServer::CUNSoapServer(){}

    CUNSoapServer::~CUNSoapServer(){}

    void CUNSoapServer::Delete(){}
}
}