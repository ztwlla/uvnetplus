# uvNetPlus
+ 基于libuv实现一些网络协议的封装

## 编译
+ windows使用vs2012
+ linux使用makefile

## 协议实现情况

| 协议 | 客户端 | 服务端 |
|----- |----- |----- |
| tcp(tls) | yes | yes |
| tcppool(tls) | yes | no |
| http(https) | yes | yes |
| ws(wss) | no | no |
| soap | yes | no |
| ftp(ftps) | yes | no |
| udp | no | no |
| kcp | no | no |

## demo
* https://gitee.com/ztwlla/uvMoudles.git
* https://github.com/BigPig0/uvMoudles.git

## 第三方库
* libUV: https://github.com/libuv/libuv.git
