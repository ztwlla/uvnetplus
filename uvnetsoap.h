#pragma once
#include "uvnetpuclic.h"
#include "uvnetplussoap.h"
#include <string>
#include <stdint.h>
#include <unordered_map>

namespace uvNetPlus {
namespace Http {

    struct WsdlMessage {
        std::string name;
        std::unordered_map<std::string, std::string> parts;
    };

    struct WsdlPortType {
        struct WsdlOperation {
            struct WsdlsoapBody{
                std::string name;
                std::string message;
            };
            std::string name;
            WsdlsoapBody input;
            WsdlsoapBody output;
        };
        std::string name;
        std::unordered_map<std::string, WsdlOperation*> operations;
    };

    struct WsdlBinding {
        struct WsdlOperation {
            struct WsdlsoapBody{
                std::string name;
                std::string use;
                std::string encodingStyle;
                std::string nameSpace;
            };
            std::string name;
            WsdlsoapBody input;
            WsdlsoapBody output;
        };
        std::string name;
        std::string type;
        std::unordered_map<std::string, WsdlOperation*> operations;
    };

    struct WsdlService {
        struct WsdlPort {
            std::string name;
            std::string bindingName;
            std::string address;
        };
        std::string name;
        std::unordered_map<std::string, WsdlPort*> ports; 
    };

    class CWsdl {
    public:
        //解析wsdl内容
        bool ParseResponse();

        std::string     m_recvData; //wsdl
    protected:
        std::unordered_map<std::string, std::string>
            m_mapRootAttributes;
        std::unordered_map<std::string, WsdlService*>
            m_mapServices;
        std::unordered_map<std::string, WsdlBinding*>
            m_mapBinds;
        std::unordered_map<std::string, WsdlPortType*>
            m_mapPortTypes;
        std::unordered_map<std::string, WsdlMessage*>
            m_mapMessages;
    };

    //////////////////////////////////////////////////////////////////////////

    class CUNSoapRequest : public CSoapRequest {
    public:
        CUNSoapRequest();
        virtual bool SetParam(std::string name, std::string value, CODE_METHOD codeMethod = CODE_METHOD_NONE);
        virtual void Request(CSoapRequest::RequestCB cb, void* usrData);


        WsdlBinding::WsdlOperation *pBindingOperation;
        WsdlPortType::WsdlOperation *pPortOperation;

        std::string         m_strHost;
        uint32_t            m_nPort;
        std::string         m_strPath;

        std::string         m_xmlnsSOAPENV;
        std::string         m_xmlnsSOAPENC;
        std::string         m_xmlnsXsi;
        std::string         m_xmlnsXsd;
        std::string         m_xmlnsNamespace;

        std::string         m_strEnvelopeHead; //xml头部
        std::string         m_strEnvelopeTail; //xml结尾

        unordered_map<std::string, std::string>
                            m_mapParams;
    };

    //////////////////////////////////////////////////////////////////////////

    class CUNSoapResponse : public CSoapResponse {
    public:
        CUNSoapResponse();
    };

    //////////////////////////////////////////////////////////////////////////

    class CUNSoapClient : public CSoapClient, public CWsdl {
    public:
        CUNSoapClient();
        ~CUNSoapClient();

        virtual void Delete();

        virtual CSoapRequest* Request(std::string service, std::string port, std::string operation);

    public:
        CHttpClient    *m_pHttpClient;
        ParseCB         m_funParseWsdlCB;
        std::string     m_strHost;
        uint32_t        m_nPort;
        std::string     m_strPath;
    };

    class CUNSoapServer : public CSoapServer, public CWsdl {
    public:
        CUNSoapServer();
        ~CUNSoapServer();

        virtual void Delete();

    public:
        CHttpServer    *m_pHttpServer;
        ParseCB         m_funParseWsdlCB;
    };
}
}