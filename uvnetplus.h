#pragma once
#include "uvnetpuclic.h"
#include <string>
#include <list>
#include <vector>
#include <unordered_map>
#include <map>
#include <stdint.h>

namespace uvNetPlus {

/** 事件循环eventloop执行线程，封装uv_loop */
class CNet
{
public:
    /**
     * 创建一个封装的loop事件循环
     * loop 从外部传入时内部不创建事件线程，为空时内部自己创建loop及事件线程
     */
    static CNet* Create(void *loop = NULL);
    virtual ~CNet(){};
    virtual void* Loop() = 0;
protected:
    CNet(){};
};

}//namespace uvNetPlus


#include "uvnetplustcp.h"
#include "uvnetplustcppool.h"
#include "uvnetplushttp.h"
#include "uvnetplussoap.h"
#include "uvnetplusftp.h"