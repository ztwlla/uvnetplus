#pragma once

namespace uvNetPlus {
    class CTcpSocket;
    class CTcpServer;
    class CTcpAgent;

    /** TCP客户端 */
class CTcpSocket
{
    typedef void (*EventCB)(CTcpSocket* skt);
    typedef void (*RecvCB)(CTcpSocket* skt, char *data, int len);
    typedef void (*ErrorCB)(CTcpSocket* skt, std::string error);
public:
    EventCB      OnReady;     //socket创建完成
#ifdef USE_TLS
    EventCB      OnTlsReady;  //TLS通道建立完成
#endif
    ErrorCB      OnConnect;   //连接完成
    RecvCB       OnRecv;      //收到数据
    EventCB      OnDrain;     //发送队列全部完成
    EventCB      OnCLose;     //socket关闭
    EventCB      OnEnd;       //收到对方fin,读到eof
    EventCB      OnTimeout;   //超时回调
    ErrorCB      OnError;     //错误回调

    bool         autoRecv;    //连接建立后是否立即自动接收数据。默认true
    bool         copy;        //发送的数据拷贝到临时区域
    void        *userData;    //用户绑定自定义数据
    uint64_t     fd;          // SOCKET的值(测试用)
    std::string  tag;         // 给tcp实例一个备注(测试用)

    /**
     * 创建一个tcp连接客户端实例
     * @param net 环境句柄
     * @param usr 设定用户自定义数据
     * @param copy 调用发送接口时，是否将数据拷贝到缓存由内部进行管理
     */
    static CTcpSocket* Create(CNet* net, void *usr=nullptr, bool copy=true);

#ifdef USE_TLS
    /**
     * 开启tls
     * @host，验证证书时必须匹配
     * @verify 证书是否必须验证,默认不需要验证
     */
    virtual void UseTls(std::string host, bool verify = false) = 0;
    /**
     * 指定一个pem格式的CA
     */
    virtual void SetTlsCa(const uint8_t *buff, uint64_t buffLen) = 0;
    /**
     * 添加多个pem格式的CA文件
     * @path pem文件的路径，字符串的数组，以NULL结尾
     */
    virtual void SetTlsCaFile(const char **path) = 0;
    /**
     * 添加指定目录下所有的pem格式的CA文件
     * @path 目录路径，字符串的数组，以NULL结尾
     */
    virtual void SetTlsCaPath(const char **path) = 0;
#endif

    /**
     * 异步删除这个实例
     */
    virtual void Delete() = 0;

    /**
     * 连接服务器，连接完成后调用OnConnect回调
     */
    virtual void Connect(std::string strIP, uint32_t nPort) = 0;

    /**
     * 设置socket的本地端口，如果不指定，将有系统自动分配
     * @param strIP 本地IP，用来指定本定使用哪一个网卡。空表示不指定。
     * @param nPort 本定端口，0表示不指定
     */
    virtual void SetLocal(std::string strIP, uint32_t nPort) = 0; 

    /**
     * 建立一个socket后，可以获取其本地地址
     */
    virtual void GetLocal(std::string &strIP, uint32_t &nPort) = 0;

    /**
     * 发送数据。将数据放到本地缓存起来
     */
    virtual void Send(const char *pData, uint32_t nLen) = 0;

protected:
    CTcpSocket();
    virtual ~CTcpSocket() = 0;
};

/** TCP服务端 */
class CTcpServer
{
    typedef void (*EventCB)(CTcpServer* svr, std::string err);
    typedef void (*ConnCB)(CTcpServer* svr, std::string err, CTcpSocket* client);
public:

    EventCB          OnListen;       // 开启监听完成回调，错误时上抛错误消息
    ConnCB           OnConnection;   // 新连接回调
    EventCB          OnClose;        // 监听socket关闭完成回调
    EventCB          OnError;        // 发生错误回调

    void            *userData;

    /**
     * 创建一个tcp服务端实例
     * @param net 环境句柄
     * @param onConnection 指定收到新连接时的回调
     * @param usr 设定用户自定义数据
     */
    static CTcpServer* Create(CNet* net, ConnCB onConnection, void *usr=nullptr);

    /**
     * 异步删除当前实例
     */
    virtual void Delete() = 0;

    /**
     * 启动监听
     * @param strIP 本地IP，用来指定本定使用哪一个网卡
     * @param nPort 本地监听端口
     */
    virtual bool Listen(std::string strIP, uint32_t nPort) = 0;

    /** 服务器是否在监听连接 */
    virtual bool Listening() = 0;

#ifdef USE_TLS
	/**
	 * 开启Tls
	 */
    virtual void UseTls() = 0;

	/**
	 * 设置CA内容
	 */
	virtual void SetCA(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置CA文件
	 */
	virtual void SetCAFile(std::string path) = 0;

	/**
	 * 设置证书内容
	 */
	virtual void SetCrt(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置证书文件
	 */
	virtual void SetCrtFile(std::string path) = 0;

	/**
	 * 设置私钥内容
	 */
	virtual void SetKey(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置私钥文件
	 */
	virtual void SetKeyFile(std::string path) = 0;
#endif
protected:
    CTcpServer();
    virtual ~CTcpServer() = 0;
};

/** 简单的TCP连接池管理 */
class CTcpAgent
{
    typedef void (*EventCB)(CTcpAgent *agent, CTcpSocket *skt);
public:
    /** 创建连接池 */
    static CTcpAgent* Create(CNet* net);

    /** 向连接池放入一个socket */
    virtual bool Put(CTcpSocket *skt) = 0;

    /** 从连接池移除一个socket */
    virtual bool Remove(CTcpSocket *skt) = 0;

    /** 销毁连接池 */
    virtual void Delete() = 0;

    EventCB    onTimeOut;   //超时回调
    uint32_t   timeOut;     //空闲连接超时时间 秒 默认20s 0为永不超时
protected:
    CTcpAgent();
    virtual ~CTcpAgent() = 0;
};
}