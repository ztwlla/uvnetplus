#pragma once
#ifdef USE_TLS
#include "uv.h"
#include <stdint.h>
#include <list>

namespace uvNetPlus {
class CUNTcpSocket;
class CUNTcpServer;

class CTlsEngine {
protected:
    CTlsEngine();
public:
    virtual ~CTlsEngine();
    static CTlsEngine* CreateTlsClient(CUNTcpSocket* skt, string host, bool verify=false);
    virtual void Handshake() = 0;
    virtual void Recv(char *data, ssize_t len) = 0;
    virtual int Send(std::list<uv_buf_t> sendList) = 0;
    virtual bool Handshaked() = 0;    //握手是否完成

    bool            m_bVerify;     // 是否必须验证服务器证书
    string          m_strHost;     // 客户端需要设置服务器域名
    CUNTcpSocket   *m_pTcpSocket;
    CUNTcpServer   *m_pTcpServer;
    char           *m_pReadBuff;   // 接收缓存
};

}
#endif