#pragma once

namespace uvNetPlus {
    class CTcpConnPool;

/** TCP连接池 请求结构 */
struct CTcpRequest {
    std::string     host;   //请求目标域名或ip
    uint32_t        port;   //请求端口
    std::string     localaddr; //本地ip，表明使用哪一块网卡。默认空，不限制
    bool            copy;   //需要发送的数据是否拷贝到内部维护
    bool            recv;   //tcp请求是否需要接收数据
    void           *usr;    //用户自定义数据
    bool            autodel;//回调后自动删除，不需要用户手动删除。默认true。
    void           (*OnRequest)(CTcpRequest* req, CTcpSocket* skt, std::string error);

    CTcpConnPool   *pool;   //从哪一个连接池获取连接
    CTcpRequest()
        : port(80)
        , copy(true)
        , recv(true)
        , usr(NULL)
        , autodel(true)
        , OnRequest(NULL)
        , pool(NULL)
    {}
};

/** TCP客户端连接池，自动管理多个CTcpAgent */
class CTcpConnPool
{
protected:
    typedef void (*ReqCB)(CTcpRequest* req, CTcpSocket* skt, std::string error);
public:
    uint32_t   maxConns;    //最大连接数 默认512(busy+idle)
    uint32_t   maxIdle;     //最大空闲连接数 默认100
    uint32_t   timeOut;     //空闲连接超时时间 秒 默认20s 0为永不超时
    uint32_t   maxRequest;  //连接达到最大时能存放的请求数 默认0 不限制

    ReqCB      OnRequest;   //获取TCP客户端连接回调

    /**
     * 创建连接池
     * @param net loop实例
     * @param onReq 获取TCP客户端连接回调
     */
    static CTcpConnPool* Create(CNet* net, ReqCB onReq = NULL);

    /**
     * 异步删除连接池
     */
    virtual void Delete() = 0;

    /**
     * 从连接池获取一个socket。内部申请的对象，需要用户删除
     * @param host 请求目标域名或端口
     * @param port 请求目标端口
     * @param localaddr 本地ip，指定网卡，为空表示不指定
     * @param usr 绑定一个用户数据，回调时作为参数输出
     * @param copy 发送的数据是否拷贝到内部
     * @param recv 是否需要接收应答
     * @return 返回新的请求实例
     */
    virtual bool Request(std::string host, uint32_t port, std::string localaddr
        , void *usr=nullptr, bool copy=true, bool recv=true, ReqCB onReq = NULL) = 0;

    /**
     * 从连接池获取一个socket
     * @param req 请求参数结构
     */
    virtual bool Request(CTcpRequest *req) = 0;

#ifdef USE_TLS
    /** 指定一个自定义的pem */
    virtual void SetTlsCa(const uint8_t *buff, uint64_t buffLen) = 0;
    /**
     * 添加多个pem文件
     * @path pem文件的路径，字符串的数组，以NULL结尾
     */
    virtual void SetTlsCaFile(const char **path) = 0;
    /**
     * 添加指定目录下所有的pem文件
     * @path 目录路径，字符串的数组，以NULL结尾
     */
    virtual void SetTlsCaPath(const char **path) = 0;

    bool       useTls;
    bool       verify;
#endif
protected:
    CTcpConnPool();
    virtual ~CTcpConnPool() = 0;
};
}