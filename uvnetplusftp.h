#pragma once

namespace uvNetPlus {
    namespace Ftp {
enum FTP_CMD {
    FTP_CMD_ABOR = 0, //中断数据连接程序
    FTP_CMD_ACCT, //系统特权帐号
    FTP_CMD_ALLO, //为服务器上的文件存储器分配字节
    FTP_CMD_APPE, //添加文件到服务器同名文件
    FTP_CMD_CDUP, //改变服务器上的父目录
    FTP_CMD_CWD, //改变服务器上的工作目录
    FTP_CMD_DELE, //删除服务器上的指定文件
    FTP_CMD_HELP, //返回指定命令信息
    FTP_CMD_LIST, //如果是文件名列出文件信息，如果是目录则列出文件列表
    FTP_CMD_MODE, //传输模式（S=流模式，B=块模式，C=压缩模式）
    FTP_CMD_MKD, //在服务器上建立指定目录
    FTP_CMD_NLST, //列出指定目录内容
    FTP_CMD_NOOP, //无动作，除了来自服务器上的承认
    FTP_CMD_PASS, //系统登录密码
    FTP_CMD_PASV, //请求服务器等待数据连接
    FTP_CMD_PORT, //IP 地址和两字节的端口 ID
    FTP_CMD_PWD, //显示当前工作目录
    FTP_CMD_QUIT, //从 FTP 服务器上退出登录
    FTP_CMD_REIN, //重新初始化登录状态连接
    FTP_CMD_REST, //由特定偏移量重启文件传递
    FTP_CMD_RETR, //从服务器上找回（复制）文件
    FTP_CMD_RMD, //在服务器上删除指定目录
    FTP_CMD_RNFR, //对旧路径重命名
    FTP_CMD_RNTO, //对新路径重命名
    FTP_CMD_SITE, //由服务器提供的站点特殊参数
    FTP_CMD_SMNT, //挂载指定文件结构
    FTP_CMD_STAT, //在当前程序或目录上返回信息
    FTP_CMD_STOR, //储存（复制）文件到服务器上
    FTP_CMD_STOU, //储存文件到服务器名称上
    FTP_CMD_STRU, //数据结构（F=文件，R=记录，P=页面）
    FTP_CMD_SYST, //返回服务器使用的操作系统
    FTP_CMD_TYPE, //数据类型（A=ASCII，E=EBCDIC，I=binary）
    FTP_CMD_USER, //系统登录的用户名
    FTP_CMD_OPTS, //设置选项
    FTP_CONN      //tcp连接
};

enum FTP_DATA_MOD {
    FTP_DATA_MOD_PASV = 0,
    FTP_DATA_MOD_PORT
};

enum FTP_FILE_TYPE {
    FTP_FILE_TYPE_ASCII = 0,
    FTP_FILE_TYPE_EBCDIC,
    FTP_FILE_TYPE_BINARY
};

enum FTP_CHARACTER_SET {
    FTP_CHARACTER_SET_UTF8 = 0,
    FTP_CHARACTER_SET_GBK
};

// ftp返回的文件列表信息解析出的数据结构
struct CFtpFile {
    std::string rawData;    //服务器返回的数据，没有解析
    bool        isDir;      //是否为目录
    std::string permission; //权限字符串,10位，第一位d代表目录，后面代表用户、组、其他的rwx权限
    std::string owner;
    std::string group;
    uint64_t    size;
    std::string date;
    time_t      dateTime;
    std::string name;
};

//记录一次完整的通信
struct CFtpMsg {
    FTP_CMD cmd;          //请求命令
    std::string cmdParam; //参数
    std::string cmdStr;   //完整请求命令
    int replyCode;        //服务器返回码
    std::string replyStr; //返回消息内容
    CFtpMsg():replyCode(0){}
    void Init(FTP_CMD c, std::string p);
};

//ftp连接
class CFtpConnect {
protected:
    CFtpConnect();
    virtual ~CFtpConnect();

public:
    typedef void(*ReqCB)(CFtpConnect *conn, CFtpMsg *msg);
    typedef void(*SuccessCB)(CFtpConnect *conn);
    typedef void(*NameListCB)(CFtpConnect *conn, std::list<std::string> names);
    typedef void(*ListCB)(CFtpConnect *conn, std::list<CFtpFile> files);
    typedef void(*DownloadCB)(CFtpConnect *conn, char* data, uint32_t size);

    std::string         host;      // 域名或IP
    int                 port;      // 端口
    std::string         user;      // 用户名
    std::string         pwd;       // 密码
    void               *usrData;   // 用户自定义数据
    std::string         path;      // 当前目录
    FTP_DATA_MOD        dataMod;   // 被动模式还是主动模式
    FTP_CHARACTER_SET   character; // 字符编码


    /**
     * 获取当前工作目录
     */
    virtual void GetWorkingDirectory(SuccessCB cb) = 0;

    /**
     * 改变服务器上的工作目录CWD
     */
    virtual void ChangeWorkingDirectory(std::string path, SuccessCB cb) = 0;

    /**
     * 切换文件类型
     */
    virtual void SetFileType(FTP_FILE_TYPE t, SuccessCB cb) = 0;

    /**
     * 获取服务器文件名称列表NLST
     */
    virtual void NameList(NameListCB cb) = 0;

    /**
     * 获取文件信息或文件列表LIST
     */
    virtual void List(ListCB cb) = 0;

    /**
     * 下载文件
     */
    virtual void Download(std::string file, DownloadCB cb) = 0;

    /**
     * 上传文件
     */
    virtual void Upload(std::string file, char *data, int size, SuccessCB cb) = 0;

    /**
     * 创建目录
     */
    virtual void MakeDirectory(std::string path, SuccessCB cb) = 0;

    /**
     * 删除目录
     */
    virtual void RmDirectory(std::string path, SuccessCB cb) = 0;

    /**
     * 删除文件
     */
    virtual void DelFile(std::string path, SuccessCB cb) = 0;

    /**
     * 重命名文件、移动文件位置
     */
    virtual void Rename(std::string src, std::string dst, SuccessCB cb) = 0;

    /**
     * 释放连接或将连接放回连接池
     */
    virtual void Delete() = 0;
};

//ftp连接池
class CFtpConnectPool {
public:
    virtual ~CFtpConnectPool(){};

    /**
     * 从连接池获取一个连接
     */
    virtual void Get(CFtpConnect::SuccessCB onLogin, void* usrData = NULL) = 0;

    /**
     * 将连接放到连接池
     */
    virtual void GiveBack(CFtpConnect* req) = 0;

};

// ftp服务器登陆参数
struct CFtpParam {
    std::string         host;      // 域名或IP
    int                 port;      // 端口
    std::string         user;      // 用户名
    std::string         pwd;       // 密码
    void               *usrData;   // 用户自定义数据
    FTP_DATA_MOD        dataMod;   // 被动模式还是主动模式
    //FTP_CHARACTER_SET   character; // 字符编码

    CFtpConnect::SuccessCB onSuccess;
    CFtpConnect::ReqCB     onError;

    CFtpParam()
        : port(21)
        , usrData(NULL)
        , dataMod(FTP_DATA_MOD_PASV)
        //, character(FTP_CHARACTER_SET_UTF8)
        , onSuccess(NULL)
        , onError(NULL)
    {
    }
};

class CFtpClient {
public:
    /**
     * 创建一个ftp客户端环境
     * @param net 环境句柄
     */
    static CFtpClient* Create(CNet* net);
    ~CFtpClient();

    /**
     * 创建一个ftp客户端连接，并进行登陆
     * @param host 服务器地址
     * @param port 服务器端口
     * @param user 用户名
     * @param pwd  密码
     * @param onLogin ftp登陆成功后回调
     * @param onError 异常时的回调
     * @param usrData 回调中返回的CFtpRequest实例绑定的一个用户数据
     */
    virtual void Connect(std::string host, int port, std::string user, std::string pwd, CFtpConnect::SuccessCB onLogin, CFtpConnect::ReqCB onError, void* usrData = NULL) = 0;

    virtual void Connect(CFtpParam param) = 0;

    /**
     * 创建一个ftp客户端连接池，每个连接都需要登陆
     * @param host 服务器地址
     * @param port 服务器端口
     * @param user 用户名
     * @param pwd  密码
     * @param maxConns 最大连接数(预留)
     */
    virtual CFtpConnectPool* ConnectPool(std::string host, int port, std::string user, std::string pwd, CFtpConnect::ReqCB onError, uint32_t maxConns=512) = 0;

    virtual CFtpConnectPool* ConnectPool(CFtpParam param) = 0;
protected:
    CFtpClient();
};

class CFtpResponse
{

};

class CFtpServer
{
    typedef void (*EventCB)(CFtpServer* svr, std::string err);
    typedef void(*ReqCb)(CFtpServer *server, CFtpMsg *request, CFtpResponse *response);
public:
    /** 监听开启成功 */
    EventCB OnListen;
    /** 接收到一个请求 */
    ReqCb OnRequest;

    /** 创建一个实例 */
    static CFtpServer* Create(CNet* net);

    /** 设备长连接保活时间，超过保活时间而没有新请求则断开连接 */
    virtual void SetKeepAlive(uint32_t secends) = 0;
    /** 服务器启动监听 */
    virtual bool Listen(std::string strIP, uint32_t nPort) = 0;
    /** 服务器关闭 */
    virtual void Close() = 0;
    /** 服务器是否在监听连接 */
    virtual bool Listening() = 0;


#ifdef USE_TLS
	/**
	 * 开启Tls
	 */
    virtual void UseTls() = 0;

	/**
	 * 设置CA内容
	 */
	virtual void SetCA(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置CA文件
	 */
	virtual void SetCAFile(std::string path) = 0;

	/**
	 * 设置证书内容
	 */
	virtual void SetCrt(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置证书文件
	 */
	virtual void SetCrtFile(std::string path) = 0;

	/**
	 * 设置私钥内容
	 */
	virtual void SetKey(const uint8_t *buff, uint64_t buffLen) = 0;

	/**
	 * 设置私钥文件
	 */
	virtual void SetKeyFile(std::string path) = 0;
#endif
protected:
    CFtpServer();
    virtual ~CFtpServer() = 0;
};
}
}