#ifdef USE_TLS
#include "utilc.h"
#include "util.h"
#include "uvnettls.h"
#include "uvnettcp.h"

using namespace uvNetPlus;

#include "uv.h"
//#include "mbedtls/net_sockets.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"
#include "mbedtls/ssl_cache.h"

#define READBUFF_SIZE 1024*1024

namespace uvNetPlus {
class CMbedTlsEngine : public CTlsEngine
{
public:
    CMbedTlsEngine(CUNTcpSocket* skt, string host, bool verify);
    ~CMbedTlsEngine();

    virtual void Handshake();
    virtual void Recv(char *data, ssize_t len);
    virtual int Send(std::list<uv_buf_t> sendList);
    virtual bool Handshaked();

private:
    bool ProcessRecv();
    void ProcessSend();

    static int BIO_net_send( void *ctx, const unsigned char *buf, size_t len );
    static int BIO_net_recv( void *ctx, unsigned char *buf, size_t len);

    mbedtls_entropy_context     entropy;
    mbedtls_ctr_drbg_context    ctr_drbg;
    mbedtls_ssl_context         ssl;
    mbedtls_ssl_config          conf;
    mbedtls_x509_crt            cacert;
    mbedtls_pk_context          pkey;
    mbedtls_ssl_cache_context   cache;

    util::membuf    *ssl_in;     //接收到的数据先交给mbedtls解析，mbedtls将解析后数据写到ssl_in
    util::membuf    *ssl_out;    //发送的数据先交给mbedtls加密，mbedtls将加密数据写到ssl_out
};

//////////////////////////////////////////////////////////////////////////

CTlsEngine::CTlsEngine()
    : m_bVerify(false)
    , m_pTcpSocket(NULL)
    , m_pTcpServer(NULL)
{
        m_pReadBuff = (char *)calloc(READBUFF_SIZE,1);
}

CTlsEngine::~CTlsEngine(){
    SAFE_FREE(m_pReadBuff);
}

CTlsEngine* CTlsEngine::CreateTlsClient(CUNTcpSocket* skt, string host, bool verify) {
    CMbedTlsEngine *ret = new CMbedTlsEngine(skt, host, verify);
    ret->m_bVerify = verify;
    return ret;
}

static void tls_debug_f(void *ctx, int level, const char *file, int line, const char *str){
    Log::debug("[%d] %s:%04d: %s", level, file, line, str );
}

//////////////////////////////////////////////////////////////////////////

CMbedTlsEngine::CMbedTlsEngine(CUNTcpSocket* skt, string host, bool verify)
{
    m_pTcpSocket     = skt;
    m_pTcpServer     = skt->m_pSvr;
    m_strHost        = host;
    m_bVerify        = verify;

    ssl_in  = new util::membuf(true);
    ssl_out = new util::membuf(true);

    /**
     * SSL上下文初始化
     */
    mbedtls_ssl_init( &ssl );
    /**
     * 初始化SSL配置
     */
    mbedtls_ssl_config_init( &conf );
    /**
     * 初始化根证书链表
     */
    mbedtls_x509_crt_init( &cacert );
    /**
     * 初始化SSL随机字节发生器
     */
    mbedtls_ctr_drbg_init( &ctr_drbg );
    /**
     * 初始化SSL熵
     */
    mbedtls_entropy_init( &entropy );
    /**
	 * 初始化私钥
	 */
    if(m_pTcpServer != NULL) {
        mbedtls_ssl_cache_init( &cache );
        mbedtls_pk_init( &pkey );
    }

    /**
     * 设置SSL/TLS熵源,方便产生子种子
     */
    uint64_t seed[2];
    seed[0] = uv_hrtime();
    seed[1] = uv_hrtime();
    int ret = mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func, &entropy,(uint8_t *)seed, sizeof(seed));

    if(m_pTcpServer == NULL) { // 客户端
        /**
         * 加载CA根证书
         */
        bool crt_parsed = false;
        if(!skt->m_lstCaPaths.empty()){
            for(auto path:skt->m_lstCaPaths) {
                // 加载指定目录下的所有证书
                ret = mbedtls_x509_crt_parse_path(&cacert, path.c_str());
                if(ret >= 0) {
                    crt_parsed = true;
                }
            }
        }
        if(!skt->m_lstCaFiles.empty()) {
            for(auto path:skt->m_lstCaFiles) {
                // 加载指定的证书文件
                ret = mbedtls_x509_crt_parse_file(&cacert, path.c_str());
                if(ret >= 0) {
                    crt_parsed = true;
                }
            }
        }
        if(!skt->m_lstCaDatas.empty()) {
            for(auto data:skt->m_lstCaDatas) {
                // 加载用户自定义的证书内容
                ret = mbedtls_x509_crt_parse( &cacert, (const unsigned char*)data.c_str(), data.size());
                if(ret >= 0) {
                    crt_parsed = true;
                }
            }
        }
        if(!crt_parsed) {
            //没有配置正确的证书，使用测试数据
            ret = mbedtls_x509_crt_parse( &cacert, (const unsigned char *)mbedtls_test_cas_pem, mbedtls_test_cas_pem_len);
        }

        /**
         * 加载默认的 SSL 配置
         * endpoint，MBEDTLS_SSL_IS_CLIENT 或者 MBEDTLS_SSL_IS_SERVER
         * transport，TLS: MBEDTLS_SSL_TRANSPORT_STREAM; DTLS: MBEDTLS_SSL_TRANSPORT_DATAGRAM
         * preset， 预定义的 MBEDTLS_SSL_PRESET_XXX 类型值，默认使用 MBEDTLS_SSL_PRESET_DEFAULT
         */
        ret = mbedtls_ssl_config_defaults(&conf, MBEDTLS_SSL_IS_CLIENT, MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
        /**
         * 设置证书验证模式默认值
         * MBEDTLS_SSL_VERIFY_NONE 不需要请求证书，单向认证时，服务器使用
         * MBEDTLS_SSL_VERIFY_REQUIRED 需要验证证书通过才能继续通讯
         * MBEDTLS_SSL_VERIFY_OPTIONAL 表示证书验证失败也可以继续通讯
         */
        mbedtls_ssl_conf_authmode(&conf, verify?MBEDTLS_SSL_VERIFY_REQUIRED:MBEDTLS_SSL_VERIFY_OPTIONAL);
        /**
         * 设置验证对等证书所需的数据
         * CRL 证书吊销列表为空表示不去判断是否注销
         */
        mbedtls_ssl_conf_ca_chain(&conf, &cacert, NULL);
    } else {        // 服务器端加载证书和私钥
        /**
         * 服务器证书
         */
        if(!m_pTcpServer->crtFile.empty()) {
            ret = mbedtls_x509_crt_parse_file(&cacert, m_pTcpServer->crtFile.c_str());
        } else if(!m_pTcpServer->crtData.empty()) {
            ret = mbedtls_x509_crt_parse(&cacert, (const unsigned char*)m_pTcpServer->crtData.c_str(), m_pTcpServer->crtData.size());
        } else {
            ret = mbedtls_x509_crt_parse(&cacert, (const unsigned char *)mbedtls_test_srv_crt, mbedtls_test_srv_crt_len);
        }

        /**
         * CA根证书
         */
        if(!m_pTcpServer->caFile.empty()) {
            ret = mbedtls_x509_crt_parse_file(&cacert, m_pTcpServer->caFile.c_str());
        } else if(!m_pTcpServer->caData.empty()) {
            ret = mbedtls_x509_crt_parse(&cacert, (const unsigned char*)m_pTcpServer->caData.c_str(), m_pTcpServer->caData.size());
        } else {
            ret = mbedtls_x509_crt_parse(&cacert, (const unsigned char*)mbedtls_test_cas_pem, mbedtls_test_cas_pem_len);
        }

        /**
         * 私钥
         */
        if(!m_pTcpServer->keyFile.empty()) {
            ret = mbedtls_pk_parse_keyfile(&pkey, m_pTcpServer->keyFile.c_str(), NULL);
        } else if(!m_pTcpServer->keyData.empty()){
            ret = mbedtls_pk_parse_key(&pkey, (const unsigned char *)m_pTcpServer->keyData.c_str(), m_pTcpServer->keyData.size(), NULL, 0);
        } else {
            ret = mbedtls_pk_parse_key(&pkey, (const unsigned char *)mbedtls_test_srv_key, mbedtls_test_srv_key_len, NULL, 0);
        }

        /**
         * 加载默认的 SSL 配置
         * endpoint，MBEDTLS_SSL_IS_CLIENT 或者 MBEDTLS_SSL_IS_SERVER
         * transport，TLS: MBEDTLS_SSL_TRANSPORT_STREAM; DTLS: MBEDTLS_SSL_TRANSPORT_DATAGRAM
         * preset， 预定义的 MBEDTLS_SSL_PRESET_XXX 类型值，默认使用 MBEDTLS_SSL_PRESET_DEFAULT
         */
        ret = mbedtls_ssl_config_defaults(&conf, MBEDTLS_SSL_IS_SERVER, MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
        /**
         * 设置证书验证模式默认值
         * MBEDTLS_SSL_VERIFY_NONE 不需要请求证书，单向认证时，服务器使用
         * MBEDTLS_SSL_VERIFY_REQUIRED 需要验证证书通过才能继续通讯
         * MBEDTLS_SSL_VERIFY_OPTIONAL 表示证书验证失败也可以继续通讯
         */
        mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_NONE);
        /**
         * 设置会话恢复
         */
        mbedtls_ssl_conf_session_cache( &conf, &cache, mbedtls_ssl_cache_get, mbedtls_ssl_cache_set );
        /**
         * 设置验证对等证书所需的数据
         * CRL 证书吊销列表为空表示不去判断是否注销
         */
        mbedtls_ssl_conf_ca_chain(&conf, cacert.next, NULL);

        mbedtls_ssl_conf_own_cert( &conf, &cacert, &pkey);
    }
    
    /**
     * 设置随机数生成器回调
     */
    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
    /**
     * 设置日志输出方法
     */
    mbedtls_ssl_conf_dbg(&conf, tls_debug_f, stdout);
    /**
     * 设置 SSL 上下文
     */
    ret = mbedtls_ssl_setup(&ssl, &conf);
    if(m_pTcpServer == NULL) {
        /**
         * 设置主机名
         * 这里设置的 hostname 必须对应服务器证书中的 common name，即 CN 字段
         */
        ret = mbedtls_ssl_set_hostname(&ssl, host.c_str());
    }

    /**
     * 设置网络层读写接口
     * 发送的数据用户通过mbedtls_ssl_write提交给mbedtls，mbedtls通过f_send将加密数据写到缓冲区，用户再将缓冲区的加密数据发送
     * 接收的加密数据用户保存到缓冲区，mbedtls通过f_recv读取加密数据，用户通过mbedtls_ssl_read读取解密后的数据
     */
    mbedtls_ssl_set_bio(&ssl, this, BIO_net_send, BIO_net_recv, NULL);

}

CMbedTlsEngine::~CMbedTlsEngine()
{
    mbedtls_x509_crt_free( &cacert );
    mbedtls_ssl_free( &ssl );
    mbedtls_ssl_config_free( &conf );
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
    if(m_pTcpServer != NULL) {
        mbedtls_pk_free( &pkey );
    }

    SAFE_DELETE(ssl_in);
    SAFE_DELETE(ssl_out);
}

void CMbedTlsEngine::Handshake(){
    int ret = mbedtls_ssl_handshake(&ssl);
    if(ret == 0) {
        if(m_bVerify) {
            int status = mbedtls_ssl_get_verify_result(&ssl);
            if (status != 0) {
                char vrfy_buf[512];
                mbedtls_x509_crt_verify_info( vrfy_buf, sizeof( vrfy_buf ), "", status );
                Log::debug("verify failed: %s", vrfy_buf);
                if(m_pTcpSocket->OnError) {
                    m_pTcpSocket->OnError(m_pTcpSocket, vrfy_buf);
                }
                return;
            }
        }
        //握手完成应答发送出去
        ProcessSend();
        //将缓存的内容先发送掉
        m_pTcpSocket->syncSend();
        //通知调用者tls建立完成
        if(m_pTcpSocket->OnTlsReady) {
            m_pTcpSocket->OnTlsReady(m_pTcpSocket);
        }
    } else if(ret == MBEDTLS_ERR_SSL_WANT_WRITE || ret == MBEDTLS_ERR_SSL_WANT_READ) {
        ProcessSend();
    }
}

void CMbedTlsEngine::Recv(char *data, ssize_t len){
    ssl_in->write((const uint8_t*)data, len);
    while(ssl_in->fsize() > 0) {
        ProcessRecv();
    }
}

int CMbedTlsEngine::Send(std::list<uv_buf_t> sendList){
    int rc = 0;
    int sent = 0;
    int total = 0;
    uint8_t *data = NULL;
    size_t len = 0;
    for(auto buf:sendList) {
        data = (uint8_t*)buf.base;
        len = buf.len;
        sent = 0;
        while (sent < (int)len) {
            rc = mbedtls_ssl_write(&ssl, data+sent, len-sent);
            if (rc >= 0) {
                sent += rc;
            } else {
                break;
            }
        }
        total += sent;
    }

    if (total > 0) {
        ProcessSend();
        rc = 0;
    }
    return total;
}

bool CMbedTlsEngine::Handshaked(){
    return ssl.state == MBEDTLS_SSL_HANDSHAKE_OVER;
}

bool CMbedTlsEngine::ProcessRecv() {
    bool to_continue = true;
    do {
        ssize_t recv = 0;
        uv_buf_t buf = uv_buf_init(m_pReadBuff, READBUFF_SIZE);

        if (ssl.state != MBEDTLS_SSL_HANDSHAKE_OVER) {
            Handshake();
            break;
        }
        if (ssl_in->fsize() == 0) {
            break;
        }
        if (buf.base == NULL || buf.len == 0) {
            recv = UV_ENOBUFS;
        } else {
            while (ssl_in->fsize() > 0 && (buf.len - (size_t)recv) > 0) {
                uint8_t *data = (uint8_t *) buf.base + (size_t)recv;
                size_t data_len = (size_t)buf.len - (size_t)recv;
                int read = mbedtls_ssl_read(&ssl, data, data_len);
                if (read < 0) {
                    break;
                }
                recv += (ssize_t)read;
            }
        }
        //回调接收的数据
        m_pTcpSocket->OnRecv(m_pTcpSocket, m_pReadBuff, (int)recv);
        to_continue = (recv >= 0);
    } while(0);
    return to_continue;
}

void CMbedTlsEngine::ProcessSend(){
    size_t avail = ssl_out->fsize();
    if (avail > 0) {
        uint8_t *buff = (uint8_t*)calloc(avail, sizeof(uint8_t));
        size_t len = ssl_out->read(buff, avail);
        uv_buf_t uvbuf = uv_buf_init((char*)buff, (unsigned int)len);
        m_pTcpSocket->syncSending(&uvbuf, 1);
    }
}

int CMbedTlsEngine::BIO_net_send( void *ctx, const unsigned char *buf, size_t len ) {
    CMbedTlsEngine *tls = (CMbedTlsEngine*)ctx;
    tls->ssl_out->write(buf, len);
    return (int)len;
}

int CMbedTlsEngine::BIO_net_recv( void *ctx, unsigned char *buf, size_t len) {
    CMbedTlsEngine *tls = (CMbedTlsEngine*)ctx;

    if (tls->ssl_in->fsize() == 0) {
        return MBEDTLS_ERR_SSL_WANT_READ;
    }
    return (int)tls->ssl_in->read(buf, len);
}

//////////////////////////////////////////////////////////////////////////

}
#endif