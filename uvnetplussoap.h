#pragma once

#include "uvnetplushttp.h"
#include <string>

namespace uvNetPlus {
namespace Http {
    class CSoapClient;
    class CSoapRequest;
    class CSoapResponse;

    enum CODE_METHOD {
        CODE_METHOD_NONE = 0,   //不做处理，必须明确输入的内容不含xml特殊字符
        CODE_METHOD_TRANS,      //将特殊字符转换掉
        CODE_METHOD_CDATA       //将内容通过cdata包裹
    };

    /**
     * Soap请求类
     */
    class CSoapRequest {
    protected:
        CSoapRequest();
    public:
        virtual ~CSoapRequest();
        typedef void(*RequestCB)(CSoapRequest* req, CSoapResponse* res, std::string error);

        /**
         * 设置请求参数
         * @param name 参数名称
         * @param value 参数的值
         * @param codeMethod 是否需要转义value，如果value中包含xml关键字，必须转义
         */
        virtual bool SetParam(std::string name, std::string value, CODE_METHOD codeMethod = CODE_METHOD_NONE) = 0;

        /**
         * 发送soap请求
         * @param cb 请求应答回调
         * @param usrData 用户绑定数据
         */
        virtual void Request(CSoapRequest::RequestCB cb, void* usrData) = 0;

        std::string      operation;
        CSoapClient     *client;
        void* userData;
    };

    /**
     * Soap应答类
     */
    class CSoapResponse {
    protected:
        CSoapResponse();
        virtual ~CSoapResponse();
    public:
        std::map<std::string, std::string> resParams;
    };

    /**
     * Soap客户端
     */
    class CSoapClient {
    protected:
        virtual ~CSoapClient();
    public:
        typedef void(*ParseCB)(CSoapClient *client, std::string error);

        /**
         * 根据wsdl创建一个soap客户端
         * @param http 传入一个http客户端，soap请求基于该http进行发送
         * @param url Soap服务的http地址
         * @param cb soap服务xml解析完成回调
         * @param user 用户自定义数据
         */
        static void Create(CHttpClient *http, std::string url, ParseCB cb, void *user = NULL);

        /**
         * 根据wsdl创建一个soap客户端
         * @param http 传入一个http客户端，soap请求基于该http进行发送
         * @param host Soap服务的域名或ip
         * @param port Soap服务的端口
         * @param port Soap服务的路径
         * @param cb soap服务xml解析完成回调
         * @param user 用户自定义数据
         */
        static void Create(CHttpClient *http, std::string host, int port, std::string path, ParseCB cb, void *user = NULL);

        /**
         * 删除该客户端
         */
        virtual void Delete() = 0;

        /**
         * 创建一个请求实体
         * @param service 需要发送的请求的服务名称
         * @param port 需要发送的请求的端口名称
         * @param operation 需要发送的请求的操作名称
         */
        virtual CSoapRequest* Request(std::string service, std::string port, std::string operation) = 0;

        void* userData;
    };

    /**
     * Soap服务端
     */
    class CSoapServer {
    protected:
        ~CSoapServer();
    public:
        typedef void(*ParseCB)(CSoapServer *client, std::string error);

        /**
         * 根据wsdl文件创建服务
         * 
         */
        static void Create(CHttpServer *http, char* wsdl, std::string path, ParseCB cb, void *user = NULL);

        static void Create(CHttpServer *http, std::string wsdl, std::string path, ParseCB cb, void *user = NULL);

        /**
         * 删除该客户端
         */
        virtual void Delete() = 0;

        void* userData;
    };
};
};