/**
 * FTP（FILE TRANSFER PROTOCOL）：文件传输协议
 * PI（protocol interpreter）：协议解析器 用户和服务器用其来解析协议，它们的具体实现分别称为用户 PI （USER-PI）和服务器PI（SERVER-PI）
 * 服务器PI（server-PI）：服务器 PI 在 L 端口“监听”用户协议解析器的连接请求并建立控制连接。它从用户 PI接收标准的 FTP 命令，发送响应，并管理服务器 DTP
 * 服务器DTP（server-DTP）：数据传输过程，在通常的“主动”状态下是用“监听”的数据端口建立数据连接。它建立传输和存储参数，并在服务器端 PI 的命令下传输数据。服务器端 DTP 也可以用于“被动”模式，而不是主动在数据端口建立连接。
 * 用户PI（user-PI）：用户协议解析器用 U 端口建立到服务器 FTP 过程的控制连接，并在文件传输时管理用户 DTP。
 * 用户DTP（user-DTP）：数据传输过程在数据端口“监听”服务器 FTP 过程的连接。
 * 控制连接：用户PI 与服务器PI 用来交换命令和响应的信息传输通道。
 * 数据连接：通过控制连接协商的模式和类型进行数据传输。
 */
#pragma once
#include "uvnetpuclic.h"
#include "uvnettcp.h"
#include <string>
#include <stdint.h>

namespace uvNetPlus {
namespace Ftp {
    class CUNFtpClient;
    class CUNFtpConnectPool;
    class CUNFtpServer;

class CUNFtpConnect : public CFtpConnect 
{
public:
    CUNFtpConnect();

    ~CUNFtpConnect();

    /**
     * 获取当前工作目录
     */
    virtual void GetWorkingDirectory(SuccessCB cb);

    /**
     * 改变服务器上的工作目录CWD
     */
    virtual void ChangeWorkingDirectory(std::string path, SuccessCB cb);

    /**
     * 切换文件类型
     */
    virtual void SetFileType(FTP_FILE_TYPE t, SuccessCB cb);

    /**
     * 获取服务器文件列表NLST
     */
    virtual void NameList(NameListCB cb);

    /**
     * 获取文件信息或文件列表LIST
     */
    virtual void List(ListCB cb);

    /**
     * 下载文件
     */
    virtual void Download(string file, DownloadCB cb);

    /**
     * 上传文件
     */
    virtual void Upload(string file, char *data, int size, SuccessCB cb);

    /**
     * 创建目录
     */
    virtual void MakeDirectory(std::string path, SuccessCB cb);

    /**
     * 删除目录
     */
    virtual void RmDirectory(std::string path, SuccessCB cb);

    /**
     * 删除文件
     */
    virtual void DelFile(std::string path, SuccessCB cb);

    /**
     * 重命名文件、移动文件位置
     */
    virtual void Rename(std::string src, std::string dst, SuccessCB cb);

    /**
     * 释放连接或将连接放回连接池
     */
    virtual void Delete();

    /** 开启数据传输 */
    void BeginDataTrans();

    /** 完成数据传输 */
    void FinishDataTrans();

    /* User-Pi收到的应答数据处理 */
    void DoReceive(const char *data, int len);

    void ParseReceive();

    /** User-Pi连接发生错误处理 */
    void DoError(string err);

    /** User-Pi数据全部发送 */
    void DoDrain();

    /** DTP连接成功，发送传输控制命令 */
    void DoDtp();

    /** 命令发送超时 */
    void DoTimeOver();

public:
    CUVNetPlus          *m_pNet;         // 事件线程句柄
    CUNTcpSocket        *m_tcpUserPI;    // user-PI
    CFtpMsg              m_ftpMsg;       // 控制命令
    std::string          m_strRecvBuff;  // 接收数据缓存
    std::string          m_strPiHost;    // 本地使用的ip
    uint32_t             m_nPiPort;      // 本地使用的端口
    uv_timer_t          *m_timer;        // 用来判断请求超时

    CUNTcpSocket        *m_tcpUserDTP;   // user-DTP
    CFtpMsg              m_ftpData;      // 传输命令
    char                *m_dtpUploadData;// DTP上传内容
    int                  m_dtpUploadSize;// DTP上传大小
    std::string          m_strRecvData;  // 接收数据缓存
    uint32_t             m_nDataPort;    // 数据传输端口
    bool                 m_bDtp226;      // dtp命令收到结束
    bool                 m_bDtpClose;    // dtp连接断开

    bool                 m_bLoginCb;     // 是否已经调用的登陆回调，在未调用之前，有一些命令收到应答后自动发起新的命令来完成初始话，否则回调
    CUNFtpConnectPool   *m_pPool;        // 如果该连接存放于池中，则指明是哪个池，否则为空
    bool                 m_close;        // 默认false，收到远端关闭时，需要改成true，以便不再重新删除m_tcpUserPI

    /** 异常回调 */
    ReqCB                OnCB;
    /** 登陆,上传文件 成功的回调 */
    SuccessCB            OnSuccess;
    /** 获取文件名称列表应答 */
    NameListCB           OnNameListCB;
    /** 获取文件列表应答 */
    ListCB               OnListCB;
    /** 下载成功回调 */
    DownloadCB           OnDownloadCB;
};

class CUNFtpConnectPool : public CFtpConnectPool
{
public:
    CUNFtpConnectPool();
    ~CUNFtpConnectPool();

    /**
     * 从连接池获取一个连接
     */
    virtual void Get(CFtpConnect::SuccessCB onLogin, void* usrData = NULL);

    /**
     * 将连接放回连接池
     */
    virtual void GiveBack(CFtpConnect* req);

    /**
     * 将一个连接从池中移除，中断的连接需要自动移除
     */
    void RemoveRequest(CUNFtpConnect* req);

    list<CUNFtpConnect*>  m_listRequests;
    uv_mutex_t          m_mutxRequests;
    std::string         host;      // 域名或IP
    int                 port;      // 端口
    std::string         user;      // 用户名
    std::string         pwd;       // 密码
    FTP_CHARACTER_SET   character; // 字符编码
    CFtpConnect::ReqCB  onError;
    CUNFtpClient       *m_pClient;
};

class CUNFtpClient: public CFtpClient
{
public:
    CUVNetPlus       *m_pNet;      //事件线程句柄

    virtual void Connect(std::string host, int port, std::string user, std::string pwd, CFtpConnect::SuccessCB onLogin, CFtpConnect::ReqCB onError, void* usrData = NULL);

    virtual void Connect(CFtpParam param);

    void Connect(CUNFtpConnectPool *pool, CFtpConnect::SuccessCB onLogin, CFtpConnect::ReqCB onError, void* usrData = NULL);

    virtual CFtpConnectPool* ConnectPool(std::string host, int port, std::string user, std::string pwd, CFtpConnect::ReqCB onError, uint32_t maxConns=512);

    virtual CFtpConnectPool* ConnectPool(CFtpParam param);
};

/** 服务端生成应答并发送 */
class CUNFtpResponse : public CFtpResponse
{

};

/** Ftp服务端连接 */
class CSvrConn {
public:
    CSvrConn();
    /** 解析http头，成功返回true，不是http头返回false */
    bool ParseHeader();
    /** 解析内容，已经接收完整内容或块返回true，否则false */
    bool ParseContent();

    CUNFtpServer   *http;
    CTcpServer      *server;
    CTcpSocket      *client;
    std::string      buff;   //接收数据缓存
    CFtpMsg        *inc;    //保存解析到的请求数据
    CUNFtpResponse *res;    //应答
    bool             parseHeader;   //请求报文中解析出http头。默认false，请求完成后要重置为false。
};

/** ftp服务 */
class CUNFtpServer : public CFtpServer
{
public:
    CUNFtpServer(CNet* net);
    ~CUNFtpServer();

    /** 设备长连接保活时间，超过保活时间而没有新请求则断开连接 */
    virtual void SetKeepAlive(uint32_t secends);
    /** 服务器启动监听 */
    virtual bool Listen(std::string strIP, uint32_t nPort);
    /** 服务器关闭 */
    virtual void Close();
    /** 服务器是否在监听连接 */
    virtual bool Listening();
#ifdef USE_TLS
    virtual void UseTls();
    virtual void SetCA(const uint8_t *buff, uint64_t buffLen);
    virtual void SetCAFile(std::string path);
    virtual void SetCrt(const uint8_t *buff, uint64_t buffLen);
    virtual void SetCrtFile(std::string path);
    virtual void SetKey(const uint8_t *buff, uint64_t buffLen);
    virtual void SetKeyFile(std::string path);
#endif

private:
    static void OnTimeOut(CTcpAgent *agent, CTcpSocket *skt);
    static void OnTcpListen(CTcpServer* svr, std::string err);
    static void OnTcpConnection(CTcpServer* svr, std::string err, CTcpSocket* client);
    static void OnSvrCltRecv(CTcpSocket* skt, char *data, int len);
    static void OnSvrCltDrain(CTcpSocket* skt);
    static void OnSvrCltClose(CTcpSocket* skt);
    static void OnSvrCltEnd(CTcpSocket* skt);
    static void OnSvrCltError(CTcpSocket* skt, string err);

private:
    int           m_nPort;      //服务监听端口
    CTcpServer   *m_pTcpSvr;    //tcp监听服务
    CTcpAgent    *m_pAgent;     //socket连接池
#ifdef WIN32
    std::unordered_multimap<std::string,CSvrConn*> m_pConns;   //所有连接的客户端请求
#else
    std::multimap<std::string,CSvrConn*> m_pConns;   //所有连接的客户端请求
#endif
};
}
}